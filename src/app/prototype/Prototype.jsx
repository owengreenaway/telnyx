import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Prototype extends Component {
  render() {
    return (
      <div>
        <p>// I would normally delete this file at the end.</p>
        <p>// First check the routing:</p>
        <p>My url slug is: {this.props.params.slug}</p>

        <p>// Design the blog stub:</p>
        <article>
          <h2>example blog title one</h2>
          <p className="text-muted">13/12/2017 by Owen Greenaway</p>          
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo nemo, fugiat tenetur doloremque pariatur autem minus dolore aliquid consequatur quia nihil neque soluta aut esse doloribus, laboriosam culpa mollitia iste.</p>
          <div className="text-right">
            <a href="#link" >read more</a>
          </div>
        </article>
        <article>
          <h2>example blog title one</h2>
          <p className="text-muted">13/12/2017 by Owen Greenaway</p>          
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo nemo, fugiat tenetur doloremque pariatur autem minus dolore aliquid consequatur quia nihil neque soluta aut esse doloribus, laboriosam culpa mollitia iste.</p>
          <div className="text-right">
            <a href="#link" >read more</a>
          </div>
        </article>

        <p>// Design the blog:</p>
        <article>
          <h1>example blog title one</h1>
          <p className="text-muted">13/12/2017 by Owen Greenaway</p>          
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo nemo, fugiat tenetur doloremque pariatur autem minus dolore aliquid consequatur quia nihil neque soluta aut esse doloribus, laboriosam culpa mollitia iste.</p>
        </article>
        <section className="comment mb-3">
          <div className="card">
            <div className="card-body">
              <p>Top level comment with no children</p>
              <div className="row">
                <div className="col">
                  <h6 className="card-subtitle text-muted">13/12/2017 by Owen Greenaway</h6>
                </div>
                <div className="col">
                  <div className="text-right">
                    <button type="button" className="btn btn-primary">Reply</button>
                  </div>
                </div>
              </div>
              <form className="pt-3">
                <div className="form-group">
                  <label htmlFor="commentInput">Enter your comment:</label>
                  <input type="text" className="form-control" id="commentInput" />
                </div>
                <div className="text-right">
                  <button type="submit" className="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>          
        </section>

        <section className="comment mb-3">
          <div className="card">
            <div className="card-body">
              <p>Top level comment</p>
              <div className="row">
                <div className="col">
                  <h6 className="card-subtitle text-muted">13/12/2017 by Owen Greenaway</h6>
                </div>
                <div className="col">
                  <div className="text-right">
                    <a href="#link" className="card-link">Reply</a>
                  </div>
                </div>
              </div>
              <div className="comment__children ml-3 pt-3">
                <section className="comment mb-3">
                  <div className="card">
                    <div className="card-body">
                      <p>Second level comment</p>
                      <div className="row">
                        <div className="col">
                          <h6 className="card-subtitle text-muted">13/12/2017 by Owen Greenaway</h6>
                        </div>
                        <div className="col">
                          <div className="text-right">
                            <a href="#link" className="card-link">Reply</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="comment__children">
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

Prototype.propTypes = {

};

export default Prototype;