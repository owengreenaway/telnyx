import React from 'react';
import PropTypes from 'prop-types';
import Comments from '../comments';

const Blog = ({ id, title, author, publish_date, content }) => (
  <div>
    <article>
      <h1>{title}</h1>
      <p className="text-muted">{publish_date} by {author}</p>
      <div dangerouslySetInnerHTML={{ __html: content }} />
    </article>
    <Comments
      articleId={id}
    />
  </div>
);


Blog.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  publish_date: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export default Blog;
