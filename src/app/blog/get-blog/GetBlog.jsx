import React, { Component } from 'react';
import PropTypes from 'prop-types';
import 'whatwg-fetch';
import Blog from '../blog/';

class GetBlog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showError: false,
      isLoading: true,
      id: undefined,
      title: undefined,
      author: undefined,
      publish_date: undefined,
      slug: undefined,
      description: undefined,
      content: undefined,
    };
  }

  componentDidMount() {
    this.getBlog();
  }

  getBlog = () => {
    fetch(`http://localhost:9001/posts?slug=${this.props.params.slug}`)
      .then(response => response.json()).then((array) => {
        const blog = array[0] || {};
        this.setState({
          showError: false,
          isLoading: false,
          id: blog.id,
          title: blog.title,
          author: blog.author,
          publish_date: blog.publish_date,
          slug: blog.slug,
          description: blog.description,
          content: blog.content,
        });
      }).catch(() => {
        this.setState({
          showError: true,
          isLoading: false,
        });
      });
  }

  render() {
    const { showError, isLoading, id, title, author, publish_date, content } = this.state;

    if (isLoading) {
      return null;
    }

    if (showError) {
      return (
        <div className="alert alert-danger" role="alert">
          Sorry, something went wrong getting the blog. Please try again.
        </div>
      );
    }
    return (
      <Blog
        id={id}
        title={title}
        author={author}
        publish_date={publish_date}
        content={content}
      />
    );
  }
}

GetBlog.propTypes = {
  params: PropTypes.object.isRequired,
};

export default GetBlog;
