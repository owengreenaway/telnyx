import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReplying: false,
      comment: "",
      username: "",
      isSubmitting: false,
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      isSubmitting: true,
    });
    this.props.postComment(this.props.commentId, this.state.username, this.state.comment);
  }

  allowReplying = () => {
    this.setState({ isReplying: true });
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  render() {
    const { user, date, content, commentId, allComments, postComment } = this.props;
    const { isReplying, comment, username, isSubmitting } = this.state;
    const childComments = allComments.filter(a => a.parent_id === commentId);
    return (
      <section className="comment mb-3">
        <div className="card">
          <div className="card-body">
            <p>{content}</p>
            <div className="row">
              <div className="col">
                <h6 className="card-subtitle text-muted">{date} by {user}</h6>
              </div>
              <div className="col">
                <div className="text-right">
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={this.allowReplying}
                  >Reply</button>
                </div>
              </div>
            </div>
            { isReplying ?
              <form className="pt-3" onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="usernameInput">Enter your username:</label>
                  <input
                    type="text"
                    className="form-control"
                    id="usernameInput"
                    value={username}
                    name="username"
                    onChange={this.handleChange}
                    required
                    disabled={isSubmitting}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="commentInput">Enter your comment:</label>
                  <input
                    type="text"
                    className="form-control"
                    id="commentInput"
                    value={comment}
                    name="comment"
                    onChange={this.handleChange}
                    required
                    disabled={isSubmitting}
                  />
                </div>
                <div className="text-right">
                  <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
                    {isSubmitting ? "Submitting" : "Submit"}
                  </button>
                </div>
              </form>
              : null
            }
            { childComments && childComments.length > 0 ?
              <div className="comment__children ml-3 pt-3">
                { childComments && childComments.map((childComment => (
                  <Comment
                    key={childComment.id}
                    commentId={childComment.id}
                    user={childComment.user}
                    date={childComment.date}
                    content={childComment.content}
                    allComments={allComments}
                    postComment={postComment}
                  />
                  )))}
              </div>
              : null
            }
          </div>
        </div>
      </section>
    );
  }
}

Comment.propTypes = {
  postComment: PropTypes.func.isRequired,
  user: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  commentId: PropTypes.number.isRequired,
  allComments: PropTypes.array.isRequired,
};

export default Comment;
