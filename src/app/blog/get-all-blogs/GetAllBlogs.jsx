import React, { Component } from 'react';
import 'whatwg-fetch';
import AllBlogs from '../all-blogs/';

class GetAllBlogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blogs: [],
      isLoading: true,
      showError: false,
    };
  }

  componentDidMount() {
    this.getBlogs();
  }

  getBlogs = () => {
    fetch('http://localhost:9001/posts')
      .then(response => response.json()).then((blogs) => {
        const sortedBlogs = blogs.sort((a, b) => {
          const dateA = new Date(a.publish_date);
          const dateB = new Date(b.publish_date);
          return dateB - dateA;
        });

        this.setState({
          blogs: sortedBlogs,
          showError: false,
          isLoading: false,
        });
      }).catch(() => {
        this.setState({
          showError: true,
          isLoading: true,
        });
      });
  }

  render() {
    const { isLoading, blogs, showError } = this.state;

    if (isLoading) {
      return null;
    }

    if (showError) {
      return (
        <div className="alert alert-danger" role="alert">
          Sorry, something went wrong getting the blogs. Please try again.
        </div>
      );
    }

    return (
      <AllBlogs
        blogs={blogs}
      />
    );
  }
}

export default GetAllBlogs;
