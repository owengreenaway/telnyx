import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const AllBlogs = ({ blogs }) => {
  if (blogs === null || blogs === undefined) {
    return (
      <div className="alert alert-danger" role="alert">
        Oops, something went wrong
      </div>
    );
  }

  if (blogs && blogs.length === 0) {
    return (
      <div className="alert alert-danger" role="alert">
        We're just writing our first blog, please come back later
      </div>
    );
  }

  return (
    <div>
      { blogs && blogs.map((blog) => {
        const { author, description, title, id, publish_date, slug } = blog;
        return (
          <article key={id.toString()}>
            <h2>{title}</h2>
            <p className="text-muted">{publish_date} by {author}</p>
            <p>{description}</p>
            <div className="text-right">
              <Link to={`/blog/${slug}`}>
                <span>read more</span>
                <span className="sr-only"> about {title}</span>
              </Link>
            </div>
          </article>
        );
      })}
    </div>
  );
};

AllBlogs.propTypes = {
  blogs: PropTypes.array.isRequired,
};

export default AllBlogs;
