/**
 * @overview Example spec file demonstrating a Jasmine test.
 *
 * @see {@link https://jasmine.github.io/2.8/introduction}
 */

import React from 'react';
import ReactTestUtils from 'react-test-renderer/shallow';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import AllBlogs from './AllBlogs';

Enzyme.configure({ adapter: new Adapter() });

describe('AllBlogs', function() {
  const oneBlog = [{
    "id": 1,
    "title":"Blog post #1",
    "author": "Melissa Manges",
    "publish_date": "2016-02-23",
    "slug": "blog-post-1",
    "description": "Utroque denique invenire et has.",
    "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
  }];

  const twoBlogs = [{
    "id": 1,
    "title":"Blog post #1",
    "author": "Melissa Manges",
    "publish_date": "2016-02-23",
    "slug": "blog-post-1",
    "description": "Utroque denique invenire et has.",
    "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
  }, {
    "id": 2,
    "title":"Blog post #2",
    "author": "Olene Ogan",
    "publish_date": "2016-03-16",
    "slug": "blog-post-2",
    "description": "Ex legere perpetua electram vim, per nisl inermis quaestio ea.",
    "content": "<p>Ex legere perpetua electram vim, per nisl inermis quaestio ea. Everti adolescens ut nec. Quod labitur assueverit vis at, sea an erat modus delicata.</p> <p>Dico omnesque epicurei te vix. Tota verterem temporibus eu quo, eu iudicabit repudiandae sea. Elitr nihil gloriatur vis in.</p>"
  }];

  const createComponent = (blogs) => {
    const renderer = ReactTestUtils.createRenderer();
    renderer.render(<AllBlogs blogs={blogs}/>);
    const component = renderer.getRenderOutput();
    return component;
  }

  it('renders the error message if blogs is null', function() {
    const component = createComponent(null);
    expect(component.props.children).toEqual("Oops, something went wrong");
  });

  it('renders the error message if blogs is undefined', function() {
    const component = createComponent(undefined);
    expect(component.props.children).toEqual("Oops, something went wrong");
  });

  it('renders the error message if blogs is empty', function() {
    const component = createComponent([]);
    expect(component.props.children).toEqual("We're just writing our first blog, please come back later");
  });

  it('renders one blog if blogs.length is 1', function() {
    const component = createComponent(oneBlog);
    expect(component.props.children.length).toEqual(1);
  });

  it('renders two blog if blogs.length is 2', function() {
    const component = createComponent(twoBlogs);
    expect(component.props.children.length).toEqual(2);
  });

  it('renders a h2 tag containing the dynamic title', function() {
    const wrapper = shallow(<AllBlogs blogs={oneBlog} />);
    expect(wrapper.find("h2").text()).toEqual(oneBlog[0].title);
  });

  it('renders the dynamic date and author', function() {
    const wrapper = shallow(<AllBlogs blogs={oneBlog} />);
    expect(wrapper.find(".text-muted").text()).toEqual("2016-02-23 by Melissa Manges");
  });

  it('creates the correct link', function() {
    const wrapper = shallow(<AllBlogs blogs={oneBlog} />);
    const link = wrapper.find("Link");
    expect(link.props().to).toEqual("/blog/blog-post-1");
  });
});
