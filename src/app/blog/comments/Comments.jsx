import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Comment from '../comment';

class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allComments: [],
      topLevelComments: [],
      isLoading: true,
      showGetError: false,
      showPostError: false,
      showPostSuccessMessage: false,
      isSubmitting: false,
      comment: "",
      username: "",
    };
  }

  componentDidMount() {
    this.getComments();
  }

  getComments = () => {
    this.setState({ isLoading: true });
    fetch(`http://localhost:9001/posts/${this.props.articleId}/comments`)
      .then(response => response.json()).then((allComments) => {
        const topLevelComments = allComments.filter(a => a.parent_id === null);
        this.setState({
          allComments,
          topLevelComments,
          showGetError: false,
          isLoading: false,
        });
      }).catch(() => {
        this.setState({
          showGetError: true,
          isLoading: false,
        });
      });
  }

  postComment = (parent_id, user, content) => {
    const now = new Date();
    const date = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`;
    const data = {
      parent_id,
      user,
      date,
      content,
    };
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    fetch(`http://localhost:9001/posts/${this.props.articleId}/comments`, {
      method: 'POST',
      headers,
      body: JSON.stringify(data),
    })
      .then(() => {
        this.getComments();
        this.setState({
          showPostError: false,
          showPostSuccessMessage: true,
          isSubmitting: false,
          username: "",
          comment: "",
        });
      }).catch(() => {
        this.setState({
          showPostError: false,
          isLoading: false,
          showPostSuccessMessage: false,
          isSubmitting: false,
        });
      });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.postComment(null, this.state.username, this.state.comment);
    this.setState({
      isSubmitting: true,
    });
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  render() {
    const {
      allComments, topLevelComments, isLoading,
      showGetError, showPostError, showPostSuccessMessage,
      username, comment, isSubmitting,
    } = this.state;
    const { postComment } = this;

    if (isLoading) {
      return (
        <p>Loading...</p>
      );
    }

    if (showGetError) {
      return (
        <div className="alert alert-danger" role="alert">
          Sorry, something went wrong getting the comments. Please try again.
        </div>
      );
    }

    return (
      <div>
        { showPostError &&
          <div className="alert alert-danger" role="alert">
            Sorry, something went wrong and your comment wasn't saved. Please try again.
          </div>
        }

        { showPostSuccessMessage &&
          <div className="alert alert-success" role="alert">
            Your comment was succesfully posted
          </div>
        }

        <div className="card mb-3">
          <div className="card-body">
            <form className="pt-3" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="usernameInput">Enter your username:</label>
                <input
                  type="text"
                  className="form-control"
                  id="usernameInput"
                  value={username}
                  name="username"
                  onChange={this.handleChange}
                  required
                  disabled={isSubmitting}
                />
              </div>
              <div className="form-group">
                <label htmlFor="commentInput">Enter your comment:</label>
                <input
                  type="text"
                  className="form-control"
                  id="commentInput"
                  value={comment}
                  name="comment"
                  onChange={this.handleChange}
                  required
                  disabled={isSubmitting}
                />
              </div>
              <div className="text-right">
                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
                  {isSubmitting ? "Submitting" : "Submit"}
                </button>
              </div>
            </form>
          </div>
        </div>

        { topLevelComments && topLevelComments.map((topLevelComment => (
          <Comment
            key={topLevelComment.id}
            commentId={topLevelComment.id}
            user={topLevelComment.user}
            date={topLevelComment.date}
            content={topLevelComment.content}
            allComments={allComments}
            postComment={postComment}
          />
          )))}
      </div>
    );
  }
}

Comments.propTypes = {
  articleId: PropTypes.number.isRequired,
};

export default Comments;
