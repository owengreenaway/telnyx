/**
 * @overview Application entry point.
 */

// Global application styles
import 'src/app.scss';

// React
import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, Redirect, browserHistory } from 'react-router';

// Our app
import App from './app/App';
import About from './app/about';
import Home from './app/home';
import Prototype from './app/prototype';
import GetAllBlogs from './app/blog/get-all-blogs/';
import GetBlog from './app/blog/get-blog/';

render((
  <Router history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={Home}/>
      <Route path='about' component={About}/>
      <Route path='home' component={Home}/>
      <Route path='prototype/:slug' component={Prototype}/>
      <Route path='blog' component={GetAllBlogs}/>
      <Route path='blog/:slug' component={GetBlog}/>
      <Redirect from='*' to='/home'/>
    </Route>
  </Router>
), document.getElementById('root'));
